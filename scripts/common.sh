#!/bin/sh

set -e

for CMD in git npm jq; do
  if ! [ -x "$(command -v ${CMD})" ]; then
    echo "ERROR: $CMD is not installed." >&2
    exit 1
  fi
done
